#!/usr/bin/bash
rosservice call /reset
rosservice call /turtle1/teleport_absolute 3.0 3.0 0.0
rosservice call /clear
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
		-- '[4.0, 0.0, 0.0]' '[0.0, 0.0, 3.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
		-- '[4.0, 0.0, 0.0]' '[0.0, 0.0,-3.0]'
rosservice call /spawn 6.0 3.0 0.0 'turtle2'
rosservice call /turtle2/set_pen 255 0 0 1 0
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
	        -- '[0.0, 4.0, 0.0]' '[0.0, 0.0,0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
	        -- '[3.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
	        -- '[-3.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
                -- '[0.0, -1.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
                -- '[2.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'



