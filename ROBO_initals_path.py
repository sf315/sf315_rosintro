ypoints = []

wpose = move_group.get_current_pose().pose
wpose.position.y += scale * 0.05  
waypoints.append(copy.deepcopy(wpose))
wpose.position.z += scale * 0.05  
waypoints.append(copy.deepcopy(wpose))
wpose.position.y -= scale * 0.05  
waypoints.append(copy.deepcopy(wpose))
wpose.position.z += scale * 0.05  
waypoints.append(copy.deepcopy(wpose))
wpose.position.y += scale * 0.05  
waypoints.append(copy.deepcopy(wpose))


wpose.position.y += scale * 0.15   
waypoints.append(copy.deepcopy(wpose))
wpose.position.y -= scale * 0.05  
waypoints.append(copy.deepcopy(wpose))
wpose.position.z -= scale * 0.05  
waypoints.append(copy.deepcopy(wpose))
wpose.position.y += scale * 0.05  
waypoints.append(copy.deepcopy(wpose))
wpose.position.y -= scale * 0.05  
waypoints.append(copy.deepcopy(wpose))
wpose.position.z -= scale * 0.05  
waypoints.append(copy.deepcopy(wpose))


# We want the Cartesian path to be interpolated at a resolution of 1 cm
# which is why we will specify 0.01 as the eef_step in Cartesian
# translation.  We will disable the jump threshold by setting it to 0.0,
# ignoring the check for infeasible jumps in joint space, which is sufficient
# for this tutorial.
(plan, fraction) = move_group.compute_cartesian_path(
    waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
)  # jump_threshold

# Note: We are just planning, not asking move_group to actually move the robot yet:
return plan, fraction

## cartesian is relative 
## go to pose is absolute
##      get end effector position by printing out current pose
## go to joint state,
## execute cartesian plan
## go to joint state
## exicute pose plan


## current pose = get group
